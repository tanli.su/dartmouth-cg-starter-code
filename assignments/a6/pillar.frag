#version 330 core
#define newsin(x) (sin(2.0 * x) * 0.5 + 0.5)

layout (std140) uniform camera
{
	mat4 projection;	/*camera's projection matrix*/
	mat4 view;			/*camera's view matrix*/
	mat4 pvm;			/*camera's projection*view*model matrix*/
	mat4 ortho;			/*camera's ortho projection matrix*/
	vec4 position;		/*camera's position in world space*/
};

struct ray{
    vec3 ori;
    vec3 dir;
};

struct sphere{
    vec3 ori;			////sphere center
    float r;			////sphere radius
};


/*uniform variables*/
uniform float iTime;					////time
uniform sampler2D tex_albedo;			////texture color
uniform sampler2D tex_normal;			////texture normal

in vec4 vtx_normal;
in vec4 vtx_uv;
in vec4 vtx_tangent;

in vec4 vtx_color; 
in vec4 vtx_pos;


out vec4 frag_color;

// const vec3 LightPosition = vec3(0, -1, 2);
// const vec3 Ia = 20*vec3(2.);
// const vec3 Id = 50*vec3(3.);
// const vec3 Is = vec3(1., 1., 1.);
// const vec3 ka = 0.1*vec3(1., 1., 1.);
// const vec3 kd = 0.7*vec3(.05, .05, .05);
// const vec3 ks = 0.7*vec3(2.);
const vec3 LightPosition = vec3(-1, -1, 2);
const vec3 Ia = vec3(1.5);
const vec3 Id = vec3(8);
const vec3 ka = vec3(1);
const vec3 kd = vec3(0.7);

const float p = 40.0;

void main()							
{		
	frag_color = vec4(1.f,0.f,0.f,1.f);

	vec4 col = texture(tex_albedo, vtx_uv.xy);
	vec3 normal_map = texture(tex_normal, vtx_uv.xy).xyz;
	normal_map = normalize(normal_map*2 - 1);

	const vec4 green = vec4(0.0, 1.1, 0.5, 1.0);
    const vec4 navy = vec4(0.0, 0.3, 0.6, 1.0);
	const vec4 purple = vec4(0.3, 0.0, 0.5, 1.0);
	float changeColor = abs(sin(0.2 * iTime));
	float t = newsin(-0.1 * iTime + vtx_uv.x * 100.0) * 0.075 + newsin(0.1 * iTime + vtx_uv.x * distance(vtx_uv.x, 0.5) * 100.0) * 0.1 - 0.5;
	t = 1.0 - smoothstep(vtx_uv.y - 4.0, vtx_uv.y * 2.0, t);
	vec4 first_color = mix(purple, green, changeColor);
	vec4 final_color = mix(first_color, navy, clamp(1.0 - vtx_uv.y * t, 0.0, 1.0));
    final_color += final_color * final_color;
    vec4 color = final_color * t * (t + 0.5) * 0.75 + 0.1;

	frag_color = vec4(col.rgb, 1);

	// normal map
	vec3 T_m = normalize(vtx_tangent.xyz);
	vec3 N_m = normalize(vtx_normal.xyz);
	vec3 B_m = normalize(cross(N_m, T_m));

	mat3 TBN = mat3(T_m, B_m, N_m);

	// lighting 
	vec3 N = normalize(TBN * normal_map.xyz);
	vec3 l_j =  normalize((LightPosition*abs(sin(cos(iTime)))+.3) - vtx_pos.xyz);
	float lambert = max(0.0 , dot(l_j,N));
	vec3 lights = (ka*Ia + lambert*kd*Id);
	vec3 L_0 = lights*color.xyz;
	L_0 = clamp (L_0, 0.0, 1.0);
	

	frag_color *= vec4(L_0, 1.0);
}	