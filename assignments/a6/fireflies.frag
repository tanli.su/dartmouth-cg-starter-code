#version 330 core
# define radius 0.01
# define sphere_Counts 70.0

layout (std140) uniform camera
{
	mat4 projection;	/*camera's projection matrix*/
	mat4 view;			/*camera's view matrix*/
	mat4 pvm;			/*camera's projection*view*model matrix*/
	mat4 ortho;			/*camera's ortho projection matrix*/
	vec4 position;		/*camera's position in world space*/
};

uniform float iTime;
uniform vec2 iResolution;

in vec4 vtx_color;
in vec4 vtx_normal; 
in vec4 vtx_position;
in vec4 gl_FragCoord;

out vec4 frag_color;

const vec3 LightPosition = vec3(3, 1, 3);

float N21(vec2 p) {
	vec3 a = fract(vec3(p.xyx) * vec3(213.897, 653.453, 253.098));
    a += dot(a, a.yzx + 79.76);
    return fract((a.x + a.y) * a.z);
}

vec2 N22(vec2 p){
    float n = N21(p);
    return vec2(n,N21(n+p));
}

void main()
{
	float alpha = 0.3;
    vec3 pointLight = vec3(0.);
    float t = 0.5 * iTime;
    for (float i=0.0; i<sphere_Counts; i++) {
        vec2 rnd = N22(vec2(i,i*2.0));
    	vec2 point = vec2(cos(t*rnd.x+i) * 20.0, sin(t*rnd.y+i) * 3.0) + vec2(2.0, 1.0);
    	float distanceToPoint = distance(vtx_position.xz, point);
    	pointLight += vec3(radius/distanceToPoint)*vec3(sin(t+i)/2.5+ 0.7);
    }
    pointLight *= vec3(1.5, 1.2, 0.5); // light yellow

	vec3 pointLight2 = vec3(0.0);
    for (float i=60.0; i<80.0;i+=1.0) {
        vec2 rnd = N22(vec2(i,i+2.0));
    	vec2 point = vec2(cos(t*rnd.x+i) * 20.0, sin(t*rnd.y+i) * 3.0) + vec2(2.0, 1.0);
    	float distanceToPoint = distance(vtx_position.xz, point);
    	pointLight2 += vec3(radius/distanceToPoint) * vec3(clamp(sin(t+i)/2.0+0.6, 0.1, 1.0));
    }
    pointLight2 *= vec3(2.0, 1.4, 0.5); // dark yellow
   	pointLight += pointLight2;

    frag_color = vec4(pointLight, alpha);
}