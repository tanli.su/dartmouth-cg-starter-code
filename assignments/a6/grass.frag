#version 330 core
#define newsin(x) (sin(2.0 * x) * 0.5 + 0.5)

/*default camera matrices. do not modify.*/
layout (std140) uniform camera
{
	mat4 projection;	/*camera's projection matrix*/
	mat4 view;			/*camera's view matrix*/
	mat4 pvm;			/*camera's projection*view*model matrix*/
	mat4 ortho;			/*camera's ortho projection matrix*/
	vec4 position;		/*camera's position in world space*/
};

struct ray{
    vec3 ori;
    vec3 dir;
};

struct sphere{
    vec3 ori;			////sphere center
    float r;			////sphere radius
};

/*uniform variables*/
uniform float iTime;					////time
uniform sampler2D tex_albedo;			////texture color
uniform sampler2D tex_normal;			////texture normal

in vec3 vtx_pos;
in vec4 vtx_color;
in vec3 vtx_normal;
in vec4 vtx_tan;
in vec4 vtx_uv;

out vec4 frag_color;

vec3 hitSphere(const ray r, const sphere s)
{
    float delta = 0.f;
    vec3 oc = r.ori - s.ori;
    float a = dot(r.dir, r.dir);
    float b = 2. * dot(oc, r.dir);
    float c = dot(oc, oc) - s.r * s.r;
    float discriminant = b * b - 4 * a * c;
    if (discriminant < 0) { 
        return vec3(0.);
    }
    delta = (-b - sqrt(discriminant)) / (2. * a);

    return r.ori + r.dir * delta;
}

// environment mapping: https://antongerdelan.net/opengl/cubemaps.html
vec3 reflected_color(vec3 normal) {
	// calculate coordinate of sky position to reflect
	vec3 incident = normalize(vtx_pos - position.xyz);
	vec3 refracted = refract(incident, normal, 1.0 / 1.33); // refraction
	ray r = ray(vtx_pos.xyz, refracted);
	sphere s = sphere(vec3(0.), 10); // skysphere centered at origin, radius 10
	vec3 sky_pos = - hitSphere(r, s); // get intersection with sky sphere
	if (length(sky_pos) == 0) {
		return vec3(0.);
	}
	// calculate uv of sky position
	float pi = 3.1415926536;
	float theta = acos(sky_pos.y / 10.); // divide by the radius
	float phi = atan(sky_pos.x, sky_pos.z);
	float u = phi / (2. * pi);
	float v = theta / pi;
	vec2 uv = vec2(u, v);

	// calculate aurora color at given uv coordinate
    const vec4 green = vec4(0.0, 1.2, 0.5, 1.0);
    const vec4 navy = vec4(0.0, 0.4, 0.6, 1.0);
	const vec4 purple = vec4(0.6, 0.0, 0.5, 1.0);
	float changeColor = abs(sin(0.2 * iTime));
    
    float t = newsin(-0.1 * iTime + uv.x * 100.0) * 0.075 + newsin(0.1 * iTime + uv.x * distance(uv.x, 0.5) * 100.0) * 0.1 - 0.5;
    t = 1.0 - smoothstep(uv.y - 4.0, uv.y * 2.0, t);

    vec4 first_color = mix(purple, green, changeColor);
	vec4 final_color = mix(first_color, navy, clamp(1.0 - uv.y * t, 0.0, 1.0));
    final_color += final_color * final_color;
    vec4 color = final_color * t * (t + 0.5) * 0.75;
	return color.xyz;
}

void main()							
{		
	vec4 tex_Color = texture(tex_albedo, vtx_uv.xy);
    if (tex_Color.a == 0.) {
		discard;
	}

	vec3 emissiveColor = tex_Color.rgb;

	vec3 lightingColor= vec3(1.,1.,1.);
	vec3 LightPosition = vec3(-3, -3, 3.5);
	vec3 LightIntensity = vec3(10);
	const vec3 ka = vec3(0.2);
	const vec3 kd = vec3(0.7);
	const vec3 ks = vec3(.2);
	const float n = 2000.;

	vec3 normal = normalize(vtx_normal.xyz);
	vec3 lightDir = LightPosition - vtx_pos;
	float _lightDist = length(lightDir);
	vec3 _lightDir = normalize(lightDir);
	vec3 _localLight = LightIntensity / (_lightDist * _lightDist);
	vec3 ambColor = ka;
	lightingColor = ambColor + kd * _localLight * max(0., dot(_lightDir, normal));

	LightPosition = vec3(-3, -9, 3.5);
	LightIntensity = vec3(10);
	lightDir = LightPosition - vtx_pos;
	_lightDist = length(lightDir);
	_lightDir = normalize(lightDir);
	_localLight = LightIntensity / (_lightDist * _lightDist);
	lightingColor += ambColor + kd * _localLight * max(0., dot(_lightDir, normal));

    vec3 c = emissiveColor * lightingColor + 0.2 * reflected_color(vtx_normal);
	frag_color = vec4(c, 1.f);
}	