/*This is your fragment shader for texture and normal mapping*/

#version 330 core
#define newsin(x) (sin(2.0 * x) * 0.5 + 0.5)

/*default camera matrices. do not modify.*/
layout (std140) uniform camera
{
	mat4 projection;	/*camera's projection matrix*/
	mat4 view;			/*camera's view matrix*/
	mat4 pvm;			/*camera's projection*view*model matrix*/
	mat4 ortho;			/*camera's ortho projection matrix*/
	vec4 position;		/*camera's position in world space*/
};

/*uniform variables*/
uniform float iTime;					////time
uniform float iResolution;					////time
uniform sampler2D tex_albedo;			////texture color
uniform sampler2D tex_normal;			////texture normal

in vec3 vtx_pos;

out vec4 frag_color;

float hash2(vec2 v)
{
	//Source : https://stackoverflow.com/questions/12964279/whats-the-origin-of-this-glsl-rand-one-liner/
	vec2 rand = vec2(0,0);
	float m = dot(v, vec2(12.9898, 78.233));
	rand =  fract(vec2(sin(m),cos(m))* 43758.5453) * 2. - 1.;

	vec2 rotateRand = vec2(0.);
	float t = 0.005 * iTime + 5.;
	rotateRand.x = cos(t) * rand.x - sin(t) * rand.y;
	rotateRand.y = sin(t) * rand.x - cos(t) * rand.y;

	return fract(rotateRand.x);
}

void stars(inout vec4 color, vec2 uv) {
	// randomizes stars
    float t = sin(0.05 * iTime * hash2(-uv)) * 0.8 + 1.0;
	// interpolation
	float c = smoothstep(0.98, 1.0, hash2(uv)) * t;
	if (c > 1.77) {
		color += c;
	}
}

void auroras(inout vec4 color, vec2 uv) {
    const vec4 green = vec4(0.0, 1.2, 0.5, 1.0);
    const vec4 navy = vec4(0.0, 0.4, 0.6, 1.0);
	const vec4 purple = vec4(0.6, 0.0, 0.5, 1.0);
	float changeColor = abs(sin(0.2 * iTime));
    
    float t = newsin(-0.1 * iTime + uv.x * 100.0) * 0.075 + newsin(0.1 * iTime + uv.x * distance(uv.x, 0.5) * 100.0) * 0.1 - 0.5;
    t = 1.0 - smoothstep(uv.y - 4.0, uv.y * 2.0, t);

    vec4 first_color = mix(purple, green, changeColor);
	vec4 final_color = mix(first_color, navy, clamp(1.0 - uv.y * t, 0.0, 1.0));
    final_color += final_color * final_color;
    color += final_color * t * (t + 0.5) * 0.75;
}

void main()							
{
	// calculate uv
	float pi = 3.1415926536;
	float theta = acos(vtx_pos.y / 10.); // divide by the radius
	float phi = atan(vtx_pos.x, vtx_pos.z);
	float u = phi / (2. * pi);
	float v = theta / pi;
	vec2 uv = vec2(u, v);
	
	// aurora and stars
	vec4 color = vec4(0.0);
	if (vtx_pos.z > 0) {
 		stars(color, uv);
		auroras(color, uv);
	}
	frag_color = vec4(color.xyz, 1.0);
}	