#version 330 core
#define newsin(x) (sin(2.0 * x) * 0.5 + 0.5)

layout (std140) uniform camera
{
	mat4 projection;	/*camera's projection matrix*/
	mat4 view;			/*camera's view matrix*/
	mat4 pvm;			/*camera's projection*view*model matrix*/
	mat4 ortho;			/*camera's ortho projection matrix*/
	vec4 position;		/*camera's position in world space*/
};

struct ray{
    vec3 ori;
    vec3 dir;
};

struct sphere{
    vec3 ori;			////sphere center
    float r;			////sphere radius
};

/*uniform variables*/
uniform sampler2D tex_albedo;			////texture color
uniform sampler2D tex_normal;			////texture normal

/*uniform variables*/
uniform float iTime;					////time
uniform vec2 iResolution;

/*input variables*/
in vec3 vtx_pos;
out vec4 frag_color;

vec2 hash2(vec2 v)
{
	// source: https://stackoverflow.com/questions/12964279/whats-the-origin-of-this-glsl-rand-one-liner/
	vec2 rand = vec2( dot(v, vec2(267.7, 150.3)), dot(v, vec2(188.6, 359.3)) );
	rand = -1. + 2. * fract(sin(rand) * 23895.723208);
	rand = normalize(rand);

	vec2 rotateRand = vec2(0.);
	float t = 0.5 * iTime;
	rotateRand.x = cos(t) * rand.x - sin(t) * rand.y;
	rotateRand.y = sin(t) * rand.x - cos(t) * rand.y;

	return rotateRand;
}

float perlin_noise(vec2 v) 
{
	float noise = 0;
	// calculate cell index i 
	float i = floor(v.x);
	float j = floor(v.y);
	vec2 ij = vec2(i, j);

	// 4 points
	vec2 p1 = vec2(i, j); // top left
	vec2 p2 = vec2(i + 1, j);  // top right
	vec2 p3 = vec2(i, j + 1); // bottom left
	vec2 p4 = vec2(i + 1, j + 1); // bottom right

	// calculate the fraction fr 
	float fx = fract(v.x);
	float fy = fract(v.y);
	vec2 fr = vec2(fx, fy);

	// smoothstep
	vec2 s = fr * fr * (3.0 - 2.0 * fr);

	float m1 = mix( dot( hash2(p1), fr - vec2(0.0,0.0) ), dot( hash2(p2), fr - vec2(1.0,0.0) ), s.x);
	float m2 = mix( dot( hash2(p3), fr - vec2(0.0,1.0) ), dot( hash2(p4), fr - vec2(1.0,1.0) ), s.x);
   
	noise = mix(m1, m2, s.y);
	return noise;
}

// noise inspired by https://www.shadertoy.com/view/MsB3WR
const mat2 m2 = mat2( 0.60, -0.80, 0.80, 0.60 );
const mat3 m3 = mat3( 0.00,  0.80,  0.60,
                     -0.80,  0.36, -0.48,
                     -0.60, -0.48,  0.64 );

float fbm(vec3 p) {
    float f = 0.0;
    f += 0.5000 * perlin_noise(p.xy); p = m3 * p * 2.02;
    f += 0.2500 * perlin_noise(p.xy); p = m3 * p * 2.03;
    f += 0.1250 * perlin_noise(p.xy); p = m3 * p * 2.01;
    f += 0.0625 * perlin_noise(p.xy);
    return f/1.5;
}

float height( vec2 pos ) {
	return abs(fbm(vec3(6. * pos * m2, iTime + 285.)) - 0.5) * 0.05 + 0.5;
}

vec3 compute_normal(vec2 v, float d)
{	
	vec3 normal_vector = vec3(0,0,0);
	vec3 v1 = vec3(v.x + d, v.y, height(vec2(v.x + d, v.y))); // right
	vec3 v2 = vec3(v.x - d, v.y, height(vec2(v.x - d, v.y))); // left
	vec3 v3 = vec3(v.x, v.y + d, height(vec2(v.x, v.y + d))); // top
	vec3 v4 = vec3(v.x, v.y - d, height(vec2(v.x, v.y - d))); // bottom

	vec3 horizontal = v1 - v2; // right - left
	vec3 vertical = v3 - v4; // top - bottom

	normal_vector = normalize(cross(horizontal, vertical));
	return normal_vector;
}

vec3 hitSphere(const ray r, const sphere s)
{
    float delta = 0.f;
    vec3 oc = r.ori - s.ori;
    float a = dot(r.dir, r.dir);
    float b = 2. * dot(oc, r.dir);
    float c = dot(oc, oc) - s.r * s.r;
    float discriminant = b * b - 4 * a * c;
    if (discriminant < 0) { 
        return vec3(0.);
    }
    delta = (-b - sqrt(discriminant)) / (2. * a);

    return r.ori + r.dir * delta;
}

// environment mapping: https://antongerdelan.net/opengl/cubemaps.html
vec3 reflected_color(vec3 normal) {
	// calculate coordinate of sky position to reflect
	vec3 incident = normalize(vtx_pos - position.xyz);
	vec3 refracted = refract(incident, normal, 1.0 / 1.33); // refraction
	ray r = ray(vtx_pos.xyz, refracted);
	sphere s = sphere(vec3(0.), 10); // skysphere centered at origin, radius 10
	vec3 sky_pos = - hitSphere(r, s); // get intersection with sky sphere
	if (length(sky_pos) == 0) {
		return vec3(0.);
	}
	// calculate uv of sky position
	float pi = 3.1415926536;
	float theta = acos(sky_pos.y / 10.); // divide by the radius
	float phi = atan(sky_pos.x, sky_pos.z);
	float u = phi / (2. * pi);
	float v = theta / pi;
	vec2 uv = vec2(u, v);

	// calculate aurora color at given uv coordinate
    const vec4 green = vec4(0.0, 1.2, 0.5, 1.0);
    const vec4 navy = vec4(0.0, 0.4, 0.6, 1.0);
	const vec4 purple = vec4(0.6, 0.0, 0.5, 1.0);
	float changeColor = abs(sin(0.2 * iTime));
    
    float t = newsin(-0.1 * iTime + uv.x * 100.0) * 0.075 + newsin(0.1 * iTime + uv.x * distance(uv.x, 0.5) * 100.0) * 0.1 - 0.5;
    t = 1.0 - smoothstep(uv.y - 4.0, uv.y * 2.0, t);

    vec4 first_color = mix(purple, green, changeColor);
	vec4 final_color = mix(first_color, navy, clamp(1.0 - uv.y * t, 0.0, 1.0));
    final_color += final_color * final_color;
    vec4 color = final_color * t * (t + 0.5) * 0.75;
	return color.xyz;
}

vec3 get_color(vec2 v)
{
	vec3 vtx_normal = compute_normal(v, 0.01);
	vec3 PURPLE_BLUE = vec3(0.1, 0.1, 0.3);	
	vec3 emissiveColor = PURPLE_BLUE;

	vec3 lightingColor= vec3(1.,1.,1.);
	const vec3 LightPosition = vec3(3, -8, 4);
	const vec3 LightIntensity = vec3(20);
	const vec3 ka = vec3(0.2);
	const vec3 kd = vec3(0.7);
	const vec3 ks = vec3(.2);
	const float n = 2000.;

	vec3 normal = normalize(vtx_normal.xyz);
	vec3 lightDir = LightPosition - vtx_pos;
	float _lightDist = length(lightDir);
	vec3 _lightDir = normalize(lightDir);
	vec3 _localLight = LightIntensity / (_lightDist * _lightDist);
	vec3 ambColor = ka;
	lightingColor = kd * _localLight * max(0., dot(_lightDir, normal));
	vec3 rj = - _lightDir + 2 * dot(_lightDir, normal.xyz) * normal.xyz;
	vec3 specular = ks * vec3(3, 2.5, 2) * pow(max(0., dot(normalize(position.xyz - vtx_pos.xyz), normalize(rj))), n);

    return emissiveColor*lightingColor + specular + 1.2 * reflected_color(vtx_normal);
}

void main()
{
	frag_color = vec4(get_color(vtx_pos.xy),1.f);
}
