/*This is your first vertex shader!*/

#version 330 core

#define PI 3.14159265

/*default camera matrices. do not modify.*/
layout (std140) uniform camera
{
	mat4 projection;	/*camera's projection matrix*/
	mat4 view;			/*camera's view matrix*/
	mat4 pvm;			/*camera's projection*view*model matrix*/
	mat4 ortho;			/*camera's ortho projection matrix*/
	vec4 position;		/*camera's position in world space*/
};

/*uniform variables*/
uniform float iTime;					////time

vec2 hash2(vec2 v)
{
	// source: https://stackoverflow.com/questions/12964279/whats-the-origin-of-this-glsl-rand-one-liner/
	vec2 rand = vec2( dot(v, vec2(267.7, 150.3)), dot(v, vec2(188.6, 359.3)) );
	rand = -1. + 2. * fract(sin(rand) * 23895.723208);
	rand = normalize(rand);

	vec2 rotateRand = vec2(0.);
	float t = 0.5 * iTime;
	rotateRand.x = cos(t) * rand.x - sin(t) * rand.y;
	rotateRand.y = sin(t) * rand.x - cos(t) * rand.y;

	return rotateRand;
}

float perlin_noise(vec2 v) 
{
	float noise = 0;
	// calculate cell index i 
	float i = floor(v.x);
	float j = floor(v.y);
	vec2 ij = vec2(i, j);

	// 4 points
	vec2 p1 = vec2(i, j); // top left
	vec2 p2 = vec2(i + 1, j);  // top right
	vec2 p3 = vec2(i, j + 1); // bottom left
	vec2 p4 = vec2(i + 1, j + 1); // bottom right

	// calculate the fraction fr 
	float fx = fract(v.x);
	float fy = fract(v.y);
	vec2 fr = vec2(fx, fy);

	// smoothstep
	vec2 s = fr * fr * (3.0 - 2.0 * fr);

	float m1 = mix( dot( hash2(p1), fr - vec2(0.0,0.0) ), dot( hash2(p2), fr - vec2(1.0,0.0) ), s.x);
	float m2 = mix( dot( hash2(p3), fr - vec2(0.0,1.0) ), dot( hash2(p4), fr - vec2(1.0,1.0) ), s.x);
   
	noise = mix(m1, m2, s.y);
	return noise;
}

// noise inspired by https://www.shadertoy.com/view/MsB3WR
const mat2 m2 = mat2( 0.60, -0.80, 0.80, 0.60 );
const mat3 m3 = mat3( 0.00,  0.80,  0.60,
                     -0.80,  0.36, -0.48,
                     -0.60, -0.48,  0.64 );

float fbm(vec3 p) {
    float f = 0.0;
    f += 0.5000 * perlin_noise(p.xy); p = m3 * p * 2.02;
    f += 0.2500 * perlin_noise(p.xy); p = m3 * p * 2.03;
    f += 0.1250 * perlin_noise(p.xy); p = m3 * p * 2.01;
    f += 0.0625 * perlin_noise(p.xy);
    return f/1.5;
}

float height( vec2 pos ) {
	return abs(fbm(vec3(6. * pos * m2, iTime + 285.)) - 0.5) * 0.05 + 0.5;
}

/*input variables*/
layout (location=0) in vec4 pos;		/*vertex position*/
layout (location=1) in vec4 color;		/*vertex color*/
layout (location=2) in vec4 normal;		/*vertex normal*/	
layout (location=3) in vec4 uv;			/*vertex uv*/		
layout (location=4) in vec4 tangent;	/*vertex tangent*/	

/*output variables*/
out vec3 vtx_pos;		////vertex position in the world space


void main()												
{
	vtx_pos = (vec4(pos.xy, height(pos.xy), 1)).xyz;
	gl_Position = pvm * vec4(vtx_pos.xyz, 1.f);
}	