//#####################################################################
// Main
// Dartmouth COSC 77/177 Computer Graphics, starter code
// Contact: Bo Zhu (bo.zhu@dartmouth.edu)
//#####################################################################
#include <iostream>
#include <random>
#include <vector>
#include <algorithm>
#include <unordered_set>
#include "Common.h"
#include "Driver.h"
#include "OpenGLMesh.h"
#include "OpenGLCommon.h"
#include "OpenGLWindow.h"
#include "OpenGLViewer.h"
#include "OpenGLMarkerObjects.h"
#include "TinyObjLoader.h"
#include "math.h"

#ifndef __Main_cpp__
#define __Main_cpp__

#ifdef __APPLE__
#define CLOCKS_PER_SEC 100000
#endif

class FinalProjectDriver : public Driver, public OpenGLViewer
{using Base=Driver;
	std::vector<OpenGLTriangleMesh*> mesh_object_array; ////mesh objects, every object you put in this array will be rendered.
	clock_t startTime;

public:
	virtual void Initialize()
	{
		draw_bk=false;						////turn off the default background and use the customized one
		draw_axes=true;						////if you don't like the axes, turn them off!
		startTime=clock();
		OpenGLViewer::Initialize();
	}

	void Add_Shaders()
	{
		////format: vertex shader name, fragment shader name, shader name
		OpenGLShaderLibrary::Instance()->Add_Shader_From_File("background.vert","background.frag","background");	
		OpenGLShaderLibrary::Instance()->Add_Shader_From_File("sphere.vert","sphere.frag","sphere");	
		OpenGLShaderLibrary::Instance()->Add_Shader_From_File("water.vert", "water.frag", "water");
		OpenGLShaderLibrary::Instance()->Add_Shader_From_File("terrain.vert", "terrain.frag", "terrain");
		OpenGLShaderLibrary::Instance()->Add_Shader_From_File("grass.vert","grass.frag","grass");
		OpenGLShaderLibrary::Instance()->Add_Shader_From_File("fireflies.vert","fireflies.frag","fireflies");	
		OpenGLShaderLibrary::Instance()->Add_Shader_From_File("pillar.vert","pillar.frag","pillar");	
	}

	void Add_Textures()
	{
		////format: image name, texture name
		OpenGLTextureLibrary::Instance()->Add_Texture_From_File("textures/grass.png", "grass_albedo");
		OpenGLTextureLibrary::Instance()->Add_Texture_From_File("textures/stone-color.jpg", "pillar_albedo");	
		OpenGLTextureLibrary::Instance()->Add_Texture_From_File("textures/stone-normal.jpg", "pillar_normal");
		OpenGLTextureLibrary::Instance()->Add_Texture_From_File("textures/rock-color.jpg", "rock_albedo");		
		OpenGLTextureLibrary::Instance()->Add_Texture_From_File("textures/rock-normal.jpg", "rock_normal");	
	}

	// Add a spherical mesh object generated analytically
	int Add_Sphere()
	{
		glDisable( GL_BLEND ); 
		auto mesh_obj=Add_Interactive_Object<OpenGLTriangleMesh>();

		real radius = 10.;
		Initialize_Sphere_Mesh(radius, &mesh_obj->mesh, 3);

		////set up shader
		mesh_obj->Add_Shader_Program(OpenGLShaderLibrary::Get_Shader("sphere"));
		Set_Polygon_Mode(mesh_obj,PolygonMode::Fill);
		Set_Shading_Mode(mesh_obj,ShadingMode::Texture);
		
		////initialize
		mesh_obj->Set_Data_Refreshed();
		mesh_obj->Initialize();	
		mesh_object_array.push_back(mesh_obj);
		return (int)mesh_object_array.size()-1;
	}

	// Add a mesh object from an obj file
	int Add_Obj_Mesh_Object(std::string obj_file_name)
	{
		auto mesh_obj=Add_Interactive_Object<OpenGLTriangleMesh>();

		Array<std::shared_ptr<TriangleMesh<3> > > meshes;
		Obj::Read_From_Obj_File_Discrete_Triangles(obj_file_name,meshes);
		mesh_obj->mesh=*meshes[0];
		// std::cout<<"load tri_mesh from obj file, #vtx: "<<mesh_obj->mesh.Vertices().size()<<", #ele: "<<mesh_obj->mesh.Elements().size()<<std::endl;		

		mesh_object_array.push_back(mesh_obj);
		return (int)mesh_object_array.size()-1;
	}

	// Add water using plane.obj
	virtual void Add_Water()
	{
		////add the plane mesh object
		int obj_idx=Add_Obj_Mesh_Object("obj/plane.obj");
		auto plane_obj=mesh_object_array[obj_idx];
		plane_obj->Add_Shader_Program(OpenGLShaderLibrary::Get_Shader("water"));

		////modify the vertices so that the plane is centered at the origin
		std::vector<Vector3>& vertices = plane_obj->mesh.Vertices();
		int vn = (int)vertices.size();
		for(int i=0; i<vn; i++){
			vertices[i] = 5 * (vertices[i] - Vector3(2.525, 2.525, 0.));
		}

		Set_Polygon_Mode(plane_obj, PolygonMode::Fill);
		Set_Shading_Mode(plane_obj, ShadingMode::Texture);
		plane_obj->Set_Data_Refreshed();
		plane_obj->Initialize();
	}

	// Add terrain using plane.obj
	virtual void Add_Terrain()
	{
		////add the plane mesh object
		int obj_idx=Add_Obj_Mesh_Object("obj/plane.obj");
		auto plane_obj=mesh_object_array[obj_idx];
		plane_obj->Add_Shader_Program(OpenGLShaderLibrary::Get_Shader("terrain"));

		////modify the vertices
		std::vector<Vector3>& vertices = plane_obj->mesh.Vertices();
		int vn = (int)vertices.size();
		for(int i=0; i<vn; i++){
			vertices[i] = 5 * (vertices[i] - Vector3(2.525, 6.0, -2.525));
		}

		Set_Polygon_Mode(plane_obj, PolygonMode::Fill);
		Set_Shading_Mode(plane_obj, ShadingMode::Texture);
		plane_obj->Set_Data_Refreshed();
		plane_obj->Initialize();
	}

	// Hash function for putting grass in random places
	Vector2 hash2(Vector2 v)
	{
		Vector2 rand = Vector2(0,0);
		rand = Vector2( v[0] * 267.7 + v[1] * 150.3, v[0] * 188.6 + v[1] * 359.3 );
		rand = Vector2(2. * sin(rand[0]) - 1., 2. * sin(rand[1]) - 1.);
		rand[0] = cos(v[0]) * rand[0] - sin(v[0]) * rand[1];
		rand[1] = sin(v[0]) * rand[0] - cos(v[0]) * rand[1];
		return rand;
	}

	virtual void Add_Grass(int x)
	{
		int obj_idx=Add_Obj_Mesh_Object("obj/three_planes.obj");
		auto grass_obj=mesh_object_array[obj_idx];
		grass_obj->Add_Shader_Program(OpenGLShaderLibrary::Get_Shader("grass"));
		grass_obj->Add_Texture("tex_albedo", OpenGLTextureLibrary::Get_Texture("grass_albedo"));

		////modify the vertices
		std::vector<Vector3>& vertices = grass_obj->mesh.Vertices();
		int vn = (int)vertices.size();
		Vector3 first = vertices[0];
		Vector2 rand = hash2(Vector2(x, x));
		for(int i=0; i<vn; i++){
			double t = rand[0] * 0.15 + 0.8;
			vertices[i] = vertices[i] * t + Vector3(0.1 * rand[0] + 0.2 * x - 4, rand[1] * 0.6 - 7.5, 1.);
		}

		Set_Polygon_Mode(grass_obj, PolygonMode::Fill);
		Set_Shading_Mode(grass_obj, ShadingMode::Texture);
		grass_obj->Set_Data_Refreshed();
		grass_obj->Initialize();	
	}

	virtual void Add_Fireflies()
	{
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA); 
		glEnable( GL_BLEND );  
		glClearColor(0.0,0.0,0.0,0.0);

		//add the plane mesh object
		int obj_idx=Add_Obj_Mesh_Object("obj/plane2.obj");
		auto plane_obj=mesh_object_array[obj_idx];
		plane_obj->Add_Shader_Program(OpenGLShaderLibrary::Get_Shader("fireflies"));

		////modify the vertices
		std::vector<Vector3>& vertices = plane_obj->mesh.Vertices();
		int vn = (int)vertices.size();
		for(int i=0; i<vn; i++){
			vertices[i] = 5 * (vertices[i] - Vector3(2.525, 0.0, -5.0));
		}

		Set_Polygon_Mode(plane_obj, PolygonMode::Fill);
		Set_Shading_Mode(plane_obj, ShadingMode::Texture);
		plane_obj->Set_Data_Refreshed();
		plane_obj->Initialize();
	}

	virtual void Add_Pillars()
	{
		auto mesh_obj=Add_Interactive_Object<OpenGLTriangleMesh>();
		std::string obj_file_name="obj/pillars.obj";
		Array<std::shared_ptr<TriangleMesh<3> > > meshes;
		Obj::Read_From_Obj_File_Discrete_Triangles(obj_file_name,meshes);
		mesh_obj->mesh=*meshes[0];

		////modify the vertices
		std::vector<Vector3>& vertices = mesh_obj->mesh.Vertices();
		int vn = (int)vertices.size();
		for(int i=0; i<vn; i++){
			vertices[i] = 0.6 * vertices[i] + Vector3(0., -5., 0.3);
		}

		////set up shader and texture
		mesh_obj->Add_Shader_Program(OpenGLShaderLibrary::Get_Shader("pillar"));
		mesh_obj->Add_Texture("tex_albedo", OpenGLTextureLibrary::Get_Texture("pillar_albedo"));
		mesh_obj->Add_Texture("tex_normal", OpenGLTextureLibrary::Get_Texture("pillar_normal"));
		Set_Polygon_Mode(mesh_obj,PolygonMode::Fill);
		Set_Shading_Mode(mesh_obj,ShadingMode::Texture);
		
		////initialize
		mesh_obj->Set_Data_Refreshed();
		mesh_obj->Initialize();	
		mesh_object_array.push_back(mesh_obj);
	}

	virtual void Add_Rocks()
	{
		auto mesh_obj=Add_Interactive_Object<OpenGLTriangleMesh>();

		////read mesh file
		std::string obj_file_name="obj/rocks.obj";
		Array<std::shared_ptr<TriangleMesh<3> > > meshes;
		Obj::Read_From_Obj_File_Discrete_Triangles(obj_file_name,meshes);
		mesh_obj->mesh=*meshes[0];

		////modify the vertices
		std::vector<Vector3>& vertices = mesh_obj->mesh.Vertices();
		int vn = (int)vertices.size();
		for(int i=0; i<vn; i++){
			vertices[i] = 0.6 * vertices[i] + Vector3(0., -5., 0.2);
		}

		////set up shader and texture
		mesh_obj->Add_Shader_Program(OpenGLShaderLibrary::Get_Shader("pillar"));
		mesh_obj->Add_Texture("tex_albedo", OpenGLTextureLibrary::Get_Texture("rock_albedo"));
		mesh_obj->Add_Texture("tex_normal", OpenGLTextureLibrary::Get_Texture("rock_normal"));
		Set_Polygon_Mode(mesh_obj,PolygonMode::Fill);
		Set_Shading_Mode(mesh_obj,ShadingMode::Texture);
		
		////initialize
		mesh_obj->Set_Data_Refreshed();
		mesh_obj->Initialize();	
		mesh_object_array.push_back(mesh_obj);
	}

	virtual void Initialize_Data()
	{
		Add_Shaders();
		Add_Textures();

		Add_Sphere();
		Add_Water();
		Add_Terrain();
		for (int i = 0; i < 50; i++) {
			Add_Grass(i);
		}
		Add_Rocks();
		Add_Pillars();
		Add_Fireflies();
	}

	////Goto next frame 
	virtual void Toggle_Next_Frame()
	{
		for (auto& mesh_obj : mesh_object_array) {
			mesh_obj->setTime(GLfloat(clock() - startTime) / CLOCKS_PER_SEC);
		}
		OpenGLViewer::Toggle_Next_Frame();
	}

	virtual void Run()
	{
		OpenGLViewer::Run();
	}
};

int main(int argc,char* argv[])
{
	FinalProjectDriver driver;
	driver.Initialize();
	driver.Run();	
}

#endif